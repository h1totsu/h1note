var gulp = require('gulp');
var bower = require('gulp-bower');
var watch = require('gulp-watch');
var mainBowerFiles = require('main-bower-files');

gulp.task('watch'), function() {
    watch('src/main/webapp/client/scripts/**/*.js', function() {
        return gulp.src('src/main/webapp/client/scripts/**/*.js').pipe(
            gulp.dest('target/noteboard/client/scripts/**/*.js'));
    });
};

gulp.task('load-vendors', ['bower-install'], function() {
    return gulp.src(mainBowerFiles(), { base: 'bower_components' })
        .pipe(gulp.dest('src/main/webapp/client/scripts/vendors'));
});


gulp.task('bower-install', function() {
    return bower();
});
