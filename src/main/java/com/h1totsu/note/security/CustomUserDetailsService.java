package com.h1totsu.note.security;

import com.h1totsu.note.domain.UserData;
import com.h1totsu.note.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserData user = userRepository.findByLogin(s);
        if (user == null) {
            throw new UsernameNotFoundException("No " + s + " user");
        } else {
            return new CustomUserDetails(user.getLogin(), user.getPassword(), user.getRole());
        }
    }
}
