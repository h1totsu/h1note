package com.h1totsu.note.repository;

import com.h1totsu.note.domain.Folder;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigDecimal;

public interface FolderRepository extends MongoRepository<Folder, BigDecimal> {
}
