package com.h1totsu.note.repository;

import com.h1totsu.note.domain.Link;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface LinkRepository extends MongoRepository<Link, BigDecimal> {
}
