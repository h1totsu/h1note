package com.h1totsu.note.repository;

import com.h1totsu.note.domain.UserData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigDecimal;

public interface UserRepository extends MongoRepository<UserData, BigDecimal> {
    UserData findByLogin(String login);
}
