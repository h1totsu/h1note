package com.h1totsu.note.rest;

import com.h1totsu.note.domain.Link;
import com.h1totsu.note.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/link")
public class LinkRestService {
    @Autowired
    private LinkRepository linkRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Link> findAll() {
        return linkRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Link findOneById(@PathVariable(value = "id") BigDecimal id) {
        return linkRepository.findOne(id);
    }
}
