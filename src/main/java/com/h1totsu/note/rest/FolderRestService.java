package com.h1totsu.note.rest;

import com.h1totsu.note.domain.Folder;
import com.h1totsu.note.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/folder")
public class FolderRestService {
    @Autowired
    private FolderRepository folderRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Folder> findAll() {
        return folderRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Folder findOneById(@PathVariable(value = "id") BigDecimal id) {
        return folderRepository.findOne(id);
    }
}
