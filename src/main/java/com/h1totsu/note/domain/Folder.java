package com.h1totsu.note.domain;

import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class Folder {
    @Id
    private BigDecimal id;
    private String name;
    private Set<BigDecimal> links = new HashSet<>();
    private Set<BigDecimal> notes = new HashSet<>();

    public Folder() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<BigDecimal> getLinks() {
        return links;
    }

    public void setLinks(Set<BigDecimal> links) {
        this.links = links;
    }

    public Set<BigDecimal> getNotes() {
        return notes;
    }

    public void setNotes(Set<BigDecimal> notes) {
        this.notes = notes;
    }
}
