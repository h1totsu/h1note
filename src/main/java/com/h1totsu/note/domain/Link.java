package com.h1totsu.note.domain;

import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Link {
    @Id
    private BigDecimal id;
    private String name;
    private String url;
    private String description;

    public Link() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
