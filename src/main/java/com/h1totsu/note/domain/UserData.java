package com.h1totsu.note.domain;

import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

public class UserData {
    @Id
    private BigDecimal id;
    private String login;
    private String password;
    private String role;

    public UserData() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
