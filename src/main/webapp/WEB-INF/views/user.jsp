<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/client/styles/main.css"/>"/>
    <script src="https://use.fontawesome.com/25cfc4b518.js"></script>
</head>
<body>
<script> var ctx = "${pageContext.request.contextPath}"</script>
<script src="<c:url value="/client/scripts/vendors/requirejs/require.js"/>"
        data-main="<c:url value="/client/scripts/main.js"/>"></script>
</body>
</html>
