define(function (require) {
    var Marionette = require('marionette'),
        dust = require('dust'),
        template = require('text!template.base.layout'),
        BoardView = require('view.base.board'),
        LeftNavView = require('view.base.left.nav'),
        HeaderView = require('view.base.header'),
        FooterView = require('view.base.footer');

    return Marionette.View.extend({
        el: 'body',

        regions: {
            header: {
                el: '.header',
                view: new HeaderView()
            },

            leftNav: {
                el: '.left-nav',
                view: new LeftNavView()
            },

            board: {
                el: '.board',
                view: new BoardView()
            },
            
            footer: {
                el: '.footer',
                view: new FooterView()
            }
        },

        childViewEvents: {
            'render:board:items': 'onRenderBoardItems'
        },

        onRenderBoardItems: function (itemType) {
            this.getRegion('board').show(new BoardView({itemType : itemType}));
        },

        
        render: function () {
            var self = this;
            dust.renderSource(template, {}, function (err, out) {
                $(self.el).html(out);
            })

            $.each(self.getRegions(), function(name, region) {
                region.show(region.options.view);
            });
        },
    });
});