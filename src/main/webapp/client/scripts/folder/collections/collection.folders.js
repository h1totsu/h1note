define(function (require) {
    var Backbone = require('backbone'),
        FolderModel = require('model.folder');

    return Backbone.Collection.extend({
        model: FolderModel,
        url: ctx + '/rest/folder',
    })
})