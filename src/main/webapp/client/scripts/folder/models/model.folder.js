define(function (require) {
    var Backbone = require('backbone');

    return Backbone.Model.extend({
        idAttribute : 'id',
        urlRoot: ctx + '/rest/folder'
    })
})