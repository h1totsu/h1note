define(function (require) {
    var Marionette = require('marionette'),
        dust = require('dust'),
        template = require('text!template.base.left.nav'),
        BoardItemTypes = require('board.item.types');

    return Marionette.View.extend({
        ui: {
            viewFolders: '._view-folders',
            viewLinks: '._view-links',
        },
        
        events: {
            'click @ui.viewLinks': 'onShowLinks',
            'click @ui.viewFolders': 'onShowFolders'
        },

        onShowLinks: function () {
            this.triggerMethod('render:board:items', BoardItemTypes.LINK);
        },

        onShowFolders: function () {
            this.triggerMethod('render:board:items', BoardItemTypes.FOLDER);
        },

        render: function () {
            var self = this;
            dust.renderSource(template, {}, function (err, out) {
                $(self.el).html(out);
            })
        },
    });
});