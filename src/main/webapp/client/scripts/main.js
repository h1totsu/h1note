requirejs.config({
    paths: {
        templates: '../templates',

        bootstrap: 'vendors/bootstrap/dist/js/bootstrap',
        jquery: 'vendors/jquery/dist/jquery',
        'jquery-sortable': 'vendors/jquery-ui-sortable/jquery-ui-sortable.min',
        underscore: 'vendors/underscore/underscore',
        backbone: 'vendors/backbone/backbone',
        'backbone.radio': 'vendors/backbone.radio/build/backbone.radio',
        'backbone.babysitter': 'vendors/backbone.babysitter/lib/backbone.babysitter',
        backbone: 'vendors/backbone/backbone',
        marionette: 'vendors/marionette/lib/core/backbone.marionette',
        dust: 'vendors/dustjs-linkedin/dist/dust-full.min',
        text: 'vendors/text/text',
    },

    shim: {
        'bootstrap': {'deps': ['jquery']},
        'jquery-sortable': {'deps': ['jquery']}
    },

    map: {
        '*': {
            'collection.links': 'link/collections/collection.links',
            'model.link': 'link/models/model.link',
            'collection.folders': 'folder/collections/collection.folders',
            'model.folder': 'folder/models/model.folder',
            
            'behaviours': 'behaviours/behaviours',
            'board.item.types': 'commons/board.item.types', 
            
            'view.base.layout': 'layout/views/view.base.layout',
            'view.base.board': 'board/views/view.base.board',
            'view.base.header': 'header/views/view.base.header',
            'view.base.footer': 'footer/views/view.base.footer',
            'view.base.left.nav': 'nav/left/view.base.left.nav',
            'view.link.item': 'link/views/view.link.item',
            'view.folder.item': 'folder/views/view.folder.item',

            'template.table.links': 'templates/link/list/template.table.links.html',
            'template.base.layout': 'templates/layout/template.base.layout.html',
            'template.base.board': 'templates/board/template.base.board.html',
            'template.base.header': 'templates/header/template.base.header.html',
            'template.base.footer': 'templates/footer/template.base.footer.html',
            'template.base.left.nav': 'templates/nav/left/template.base.left.nav.html',
            'template.link.item': 'templates/link/template.link.item.html',
            'template.folder.item': 'templates/folder/template.folder.item.html',
        },
    },
});

define.amd.dust = true;

requirejs(['bootstrap', 'application']);