define(function(require) {
    var Marionette = require('marionette'),
        dust = require('dust'),
        LinkItemView = require('view.link.item'),
        FolderItemView = require('view.folder.item'),
        LinkCollection = require('collection.links'),
        FolderCollection = require('collection.folders'), 
        Behaviors = require('behaviours'),
        BoardItemTypes = require('board.item.types');

    return Marionette.CollectionView.extend({
        tagName: 'div',
        className: 'row',

        initialize: function(options) {
            var itemType = options.itemType;
            if (itemType) {
                if (itemType === BoardItemTypes.LINK) {
                    this.collection = new LinkCollection();
                    this.childView = LinkItemView;
                } else if (itemType === BoardItemTypes.FOLDER) {
                    this.collection = new FolderCollection();
                    this.childView = FolderItemView;
                }
            }

            if (this.collection) {
                this.collection.fetch({async: false});
            }
        },

        behaviors: {
            Sortable: {
                containment: 'parent'
            }
        }
    });
});