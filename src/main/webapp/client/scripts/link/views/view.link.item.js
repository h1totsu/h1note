define(function (require) {
    var Marionette = require('marionette'),
        dust = require('dust'),
        template = require('text!template.link.item');

    return Marionette.View.extend({
        tagName: 'div',
        className: 'col-lg-2 col-md-3 col-sm-6 link-item',
        render: function () {
            var self = this;
            dust.renderSource(template, this.model.toJSON(), function (err, out) {
                $(self.el).html(out);
            })
        },
    });
});