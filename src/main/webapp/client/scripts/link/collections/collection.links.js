define(function (require) {
    var Backbone = require('backbone'),
        LinkModel = require('model.link');

    return Backbone.Collection.extend({
        model: LinkModel,
        url: ctx + '/rest/link',
    })
})