define(function (require) {
    var Marionette = require('marionette'),
        dust = require('dust'),
        template = require('text!template.base.header');

    return Marionette.View.extend({
        render: function () {
            var self = this;
            dust.renderSource(template, {}, function (err, out) {
                $(self.el).html(out);
            })
        },
    });
});